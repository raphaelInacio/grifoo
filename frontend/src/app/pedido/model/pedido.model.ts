export default class Pedido {
    public id: String
    public status: String
    public enderecoId: String
    public clienteId: String
    public eventoId: String
    public tipoPedido: String
    public mensagem: String
}